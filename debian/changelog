pd-pmpd (0.9-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Removed Hans-Christoph Steiner from Uploaders as he requested
  * Add salsa-ci configuration
  * Remove obsolete file d/source/local-options
  * Declare that building this package does not require 'root' powers.
  * Apply "warp-and-sort -ast"
  * Bump dh-compat to 13
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 04 Sep 2022 14:28:51 +0200

pd-pmpd (0.9-5) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * Depends: puredata-core | pd, so the depends is not only on a virtual package
  * Updated Upstream-Contact and made debian/copyright follow 1.0 format
  * Removed 'DM-Upload-Allowed: yes', its deprecated

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Simplified & unified d/rules
    * Enabled hardening
    * Bumped dh compat to 11
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
    * Added myself to Uploaders
  * Switched URLs to https://
  * Updated d/copyright(_hints)
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:31:14 +0100

pd-pmpd (0.9-4) unstable; urgency=low

  * updated Build-Depends to use puredata-dev (Closes: #629799)
  * bumped standards version to 3.9.2
  * Add DM-Upload-Allowed field to debian/control

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 10 Jun 2011 14:44:39 -0400

pd-pmpd (0.9-3) unstable; urgency=low

  * patched Makefile to build on kFreeBSD and Hurd (Closes: #605830)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 09 Dec 2010 23:33:12 -0500

pd-pmpd (0.9-2) unstable; urgency=low

  [ Hans-Christoph Steiner ]
  * depend on pd-libdir for lib format; recommend pd-import for loading
  * added debian/watch to keep up-to-date
  * made *Depends: have one-package-per-line format

  [ Felipe Sateler ]
  * Use the upstream provided pdf file
  * Add myself as uploader

 -- Felipe Sateler <fsateler@debian.org>  Fri, 05 Nov 2010 19:27:36 -0300

pd-pmpd (0.9-1) unstable; urgency=low

  [ Hans-Christoph Steiner ]
  * Initial release (Closes: #591709)
  * added link to pmpd.pdf in usr/share/doc/pd-pmpd/
  * remove pmpd.pdf from upstream and build one using unoconv
  * added override_dh_strip target to strip things properly
  * updated with Alessio's streamlined dh_auto_install
  * Add Vcs-* Fields

  [ Felipe Sateler ]
  * Build documentation at build time, not install time

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 12 Sep 2010 17:15:20 -0400
